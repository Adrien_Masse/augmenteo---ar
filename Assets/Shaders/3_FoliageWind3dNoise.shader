﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Cours/3_FoliageWind3dNoise" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1) // Attention necessaire pour le shadow receiver
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_EmisTex ("Emission", 2D) = "black" {}
		_EmisColor ("Color", Color) = (1,1,1,1) // Attention necessaire pour le shadow receiver
		[noscaleoffset] _BumpMap ("Normal", 2D) = "bump" {}
		[noscaleoffset] _WindMask ("WindMask", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Cutoff ("CutOut", range(0,1)) = 0.0 // Attention de bien respecter le nom de la variable _CutOff / ! \
		_WindAmount ("WindAmout", range(0,1)) = 0.0
		_WindWDir ("WindWDir", vector) = (1,0,0,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert addshadow

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _EmisTex;
		sampler2D _BumpMap;
		sampler2D _WindMask;
		fixed _Cutoff;
		fixed _WindAmount;
		float4 _WindWDir;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		fixed4 _EmisColor;

		void vert (inout appdata_full v){

			float3 _Blend = abs(mul(unity_ObjectToWorld, float4(v.normal,0.0)).xyz); // récupération des normal en absolute
			float4 _VertexGlobal = mul(unity_ObjectToWorld, v.vertex); // Multiplication de matrice => Passage des vertex local vers gloabal

			float _Mx = tex2Dlod(_WindMask, float4(_VertexGlobal.yz / 12 + _Time.x, 0.0, 0.0)); // Projection de la map de mask en x
			float _My = tex2Dlod(_WindMask, float4(_VertexGlobal.zx / 12 + _Time.x, 0.0, 0.0));
			float _Mz = tex2Dlod(_WindMask, float4(_VertexGlobal.xy / 12 + _Time.x, 0.0, 0.0));

			float _Mask = _Mx * _Blend.x + _My * _Blend.y + _Mz * _Blend.z;

			_VertexGlobal.xyz += (cos(_Time.y) * cos(_Time.z) * _WindWDir.xyz + _WindWDir.xyz) * _WindAmount * v.color.r * _Mask;
			v.vertex = mul(unity_WorldToObject, _VertexGlobal);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
			o.Emission = tex2D (_EmisTex, IN.uv_MainTex) * _EmisColor;
			clip(c.a - _Cutoff);
		}
		ENDCG
	}
	FallBack "Transparent/Cutout/Diffuse"
}
