﻿using UnityEngine;
using UnityEngine.AI;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private Camera cam;
    [SerializeField] private Transform destinationPoint;

    private NavMeshAgent agent;
    private Animator animator;
    private Vector3 startPosition;

    private void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        startPosition = transform.localPosition;
        FindObjectOfType<DefaultTrackableEventHandler>().onTargerFounded += TargetFounded;
    }

    //Commence à bouger quand le marqueur est trouvé
    private void TargetFounded (bool _value)
    {
        if(_value) {
            transform.localPosition = startPosition;
            FindObjectOfType<DefaultTrackableEventHandler>().onTargerFounded -= TargetFounded;
        }
    }

    //NavMeshAgent, se deplace là où le joueur appuie
    private void Update ()
    {
        if(Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, groundMask) && hit.collider.gameObject.layer != 9) {
                agent.isStopped = false;
                destinationPoint.position = hit.point;
                agent.SetDestination(destinationPoint.position);
            }
        }

        agent.destination = destinationPoint.position;

        animator.SetFloat("Blend", agent.velocity.magnitude / agent.speed);
    }
}