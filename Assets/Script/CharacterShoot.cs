﻿using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class CharacterShoot : MonoBehaviour
{
    [SerializeField] private Transform monster;
    [SerializeField] private float fieldOfView = 40;
    [SerializeField] private ParticleSystem[] muzzles;
    [SerializeField] private Camera cam;

    private Boss boss;

    private NavMeshAgent agent;
    private bool canShoot = false;
    public bool CanShoot { get => canShoot; set => canShoot = value; }

    private bool shootBoss;

    private void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        boss = monster.GetComponent<Boss>();
    }

    //Peu commencer à tirer que le boss a fait son apparition
    private void Update ()
    {
        if (!CanShoot) {
            if (muzzles[0].isPlaying) {
                for (int i = 0; i < muzzles.Length; i++) {
                    muzzles[i].Stop();
                }
                shootBoss = false;
            }
            return;
        }

        //Si le joueur appuie sur le boss, le personnage se tourne vers lui
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.layer == 9) {
                agent.isStopped = true;
                transform.DOLookAt(monster.position, 0.5f).OnUpdate( () => transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0));
            }
        }

        //Quand l'angle entre le personnage et le boss est ok, attaque le boss
        Vector3 targetDir = monster.position - transform.position;
        float angleToPlayer = (Vector3.Angle(targetDir, transform.forward));
        if (angleToPlayer >= -fieldOfView && angleToPlayer <= fieldOfView) {            
            if (!muzzles[0].isPlaying) {
                for (int i = 0; i < muzzles.Length; i++) {
                    muzzles[i].Play();
                }
                shootBoss = true;
            }
        }
        else {
            if (muzzles[0].isPlaying) {
                for (int i = 0; i < muzzles.Length; i++) {
                    muzzles[i].Stop();
                }
                shootBoss = false;
            }
        }
    }

    private void FixedUpdate ()
    {
        if(shootBoss)
            boss.TakeDamage();
    }
}
