﻿using UnityEngine;
using UnityEngine.AI;

public class NavigationBaker : MonoBehaviour
{    
    private NavMeshSurface surface;

    private void Start ()
    {
        surface = GetComponent<NavMeshSurface>();
        FindObjectOfType<DefaultTrackableEventHandler>().onTargerFounded += BuildNavMesh;
    }

    //Build le navmesh à chaque fois que le marqueur est trouvé
    private void BuildNavMesh (bool _value)
    {
        if(_value)
            surface.BuildNavMesh();
    }   
}