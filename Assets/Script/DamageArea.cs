﻿using UnityEngine;
using System;

public class DamageArea : MonoBehaviour
{
    public Action onTakeDamage;

    //Zone de degat pour le joueur
    private void OnTriggerEnter (Collider _col)
    {
        if(_col.gameObject.layer == 11) {
            if (onTakeDamage != null)
                onTakeDamage();
        }
    }
}
