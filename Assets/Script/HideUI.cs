﻿using UnityEngine;
using UnityEngine.UI;

public class HideUI : MonoBehaviour
{
    private Image[] images;

    private void Start ()
    {
        FindObjectOfType<DefaultTrackableEventHandler>().onTargerFounded += DisplayUI;
        images = GetComponentsInChildren<Image>();
        for (int i = 0; i < images.Length; i++) {
            images[i].enabled = false;
        }
    }

    //Cache l'UI quand le marqeur est perdu
    private void DisplayUI (bool _value)
    {
        for (int i = 0; i < images.Length; i++) {
            images[i].enabled = _value;
        }
    }
}
