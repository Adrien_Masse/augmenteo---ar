﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Boss : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private GameObject attack;
    [SerializeField] private Image lifeUI;
    [SerializeField] private float receivedDamage = 0.5f;

    private Animator animator;
    private ParticleSystem attack01Particle;
    private Collider attack01Collider;
    private Coroutine bossRoutine;
    private Coroutine attackRoutine;

    private bool timer;
    private bool playStartAnimation = true;
    private float life = 1000;

    private void Start()
    {
        animator = GetComponent<Animator>();
        transform.position = new Vector3(1000, 1000);
        FindObjectOfType<DefaultTrackableEventHandler>().onTargerFounded += StartAnimation;
        attack01Particle = attack.GetComponent<ParticleSystem>();
        attack01Collider = attack.GetComponent<Collider>();
    }

    //Démarre le combat quand le marqueur est trouvé
    private void StartAnimation (bool _value)
    {
        if(_value) {
            bossRoutine = StartCoroutine(BossCoroutine());
            attack01Collider.enabled = false;
            FindObjectOfType<DefaultTrackableEventHandler>().onTargerFounded -= StartAnimation;
        }
    }

    private IEnumerator BossCoroutine ()
    {
        //Animation de spawn
        if(playStartAnimation) {
            yield return new WaitForSeconds(5f);
            transform.localPosition = Vector3.zero;
            animator.SetTrigger("Spawn");
            playStartAnimation = false;
            StartCoroutine(Timer(5f));
            while (timer) {
                LookAtPlayer();
                yield return new WaitForEndOfFrame();
            }
            FindObjectOfType<CharacterShoot>().CanShoot = true;
        }

        //Idle, Look At le joueur
        StartCoroutine(Timer(Random.Range(2,5)));
        while (timer) {
            LookAtPlayer();
            yield return new WaitForEndOfFrame();
        }

        //Attaque avec une des 2 attaques
        State(Random.Range(1, 3));
    }

    private void LookAtPlayer ()
    {
        transform.DOLookAt(player.position, 1f);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
    }

    //2 Attaques
    private void State (int _state)
    {
        switch (_state) {
        case 1:
            animator.SetTrigger("Attack");
            attackRoutine = StartCoroutine(Attack01Coroutine());
            break;
        case 2:
            animator.SetTrigger("Attack");
            attackRoutine = StartCoroutine(Attack02Coroutine());
            break;
        default:
            break;
        }
    }

    private IEnumerator Timer (float _time)
    {
        timer = true;
        yield return new WaitForSeconds(_time);
        timer = false;
    }

    //Attaque 01, souffle des particules sur place
    private IEnumerator Attack01Coroutine ()
    {
        yield return new WaitForSeconds(0.8f);
        attack01Collider.enabled = true;
        attack01Particle.Play();
        yield return new WaitForSeconds(4);
        attack01Collider.enabled = false;
        LookAtPlayer();
        bossRoutine = StartCoroutine(BossCoroutine());
    }

    //Attaque 02, souffle des particules en pivotant
    private IEnumerator Attack02Coroutine ()
    {
        yield return new WaitForSeconds(0.8f);
        attack01Collider.enabled = true;
        attack01Particle.Play();
        int direction = Random.Range(-1, 1);
        print(direction);
        direction = direction >= 0 ? 1 : -1;
        transform.DORotate(new Vector3(0, transform.eulerAngles.y + 180 * direction, 0), 6f).OnUpdate(() => transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0));
        yield return new WaitForSeconds(6f);
        attack01Collider.enabled = false;
        LookAtPlayer();
        bossRoutine = StartCoroutine(BossCoroutine());
    }

    //Gestion des degats
    public void TakeDamage ()
    {
        life -= receivedDamage;
        lifeUI.DOFillAmount(life / 1000, 1f);
        if (life <= 0) {
            if (bossRoutine != null)
                StopCoroutine(bossRoutine);
            if (attackRoutine != null) {
                attack01Particle.Stop();
                StopCoroutine(attackRoutine);
            }
            FindObjectOfType<CharacterShoot>().CanShoot = false;
            animator.SetTrigger("Death");
        }
    }
}
