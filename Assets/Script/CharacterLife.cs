﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System.Collections;

public class CharacterLife : MonoBehaviour
{
    [SerializeField] private Image lifeUi;
    [SerializeField] private float receivedDamage = 10f;

    private float life = 100;
    private Coroutine invulnerabilityCoroutine;
    private bool canTakeDamage = true;

    private void Start ()
    {
        DamageArea[] areas = FindObjectsOfType<DamageArea>();
        for (int i = 0; i < areas.Length; i++) {
            areas[i].onTakeDamage += Damage;
        }
    }

    //Compteur de dégats
    //Recharge la scène à la mort
    private void Damage ()
    {
        if (!canTakeDamage)
            return;
        life -= receivedDamage;
        lifeUi.DOFillAmount(life / 100, 1f);
        if(invulnerabilityCoroutine == null) {
            invulnerabilityCoroutine = StartCoroutine(Invulnerability());
        }
        if (life <= 0) {
            SceneManager.LoadScene(0);
        }
    }

    private IEnumerator Invulnerability ()
    {
        canTakeDamage = false;
        yield return new WaitForSeconds(2f);
        canTakeDamage = true;
        invulnerabilityCoroutine = null;
    }
}
